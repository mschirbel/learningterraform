# Data Source

São repositórios(read-only) de informações estrangeiras ao Terraform.

Por exemplo, as AZ da AWS, podemos saber quais estão online, buscando informações diretamente da AWS.

## Exemplo

```t
data "aws_ip_ranges" "euro-ec2" {
    regions = ["eu-west-1", "eu-central-1"]
    services = ["ec2"]
}
```

Por exemplo, isso pode ser interessante caso em algum Firewall, desejemos permitir somente os IP's de certas regiões da AWS.

## Render Values

Podemos usar a estrutura de Data Source para armazenar templates para arquivos:

```t
data "template_file" "setup" {
    template = "${file("config/setup.sh")}"
}
```

## Configs

A sintaxe é a mesma, temos:

```
data "tipo" "nome" {
    "atrb" = "value"
}
```

## Demo

Dentro de terraform-tutorial/3d_Data_sources/

temos o arquivo data_sources.tf.

Veja que no nosso recurso de frontend:

```json
resource "aws_instance" "frontend" {
  tags = {
    Name = "${local.default_frontend_name}"
  }

  depends_on        = ["aws_instance.backend"]
  availability_zone = "${data.aws_availability_zones.us-east-1.names[count.index]}"
  ami               = "ami-66506c1c"
  instance_type     = "t2.micro"
}
```

Não temos mais uma lista com as AZs. Agora temos um data source, que vem da AWS, e puxa automaticamente quais estão disponíveis para subir nossa infraestrutura.

E ao usar um 

```
terraform apply
```

A primeira coisa a ser feita é recolher os valores para os Data Sources.
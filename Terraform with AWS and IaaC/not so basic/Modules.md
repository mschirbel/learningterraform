# Modules

São pacotes de configurações usados para criar elementos do Terraform.

E são gerenciados em formas de grupos, o que deixa as coisas mais fáceis.

## Uso

Podemos criar os nossos ou usar algum externo.

Para ver alguns, entre ![aqui](https://registry.terraform.io/)

Dica: Sempre procure por módulos com um tick azul, pois são verificados:

![](../images/Anotação-2019-01-02-183703.jpg)

## Instalar um módulo

É simples, basta declarar a source do módulo, como no exemplo abaixo:

```
module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"
```

E rodar o comando 

```
terraform init
```

## Criar um módulo

```
module "child" {
    source = "./child"
}
```

A partir disso, devemos criar nosso arquivos de terraform, dentro da pasta "child".

Dentro de cada pasta, obrigatoriamente, temos:

1. main.tf
2. variables.tf
3. output.tf

E assim, podemos ter módulos dentro de módulos.
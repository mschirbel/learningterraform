# Interpolation Expressions

São expressões com valores especiais dentro de strings:

```
${var.foo}
```

Podemos usar tanto com HCL ou com JSON.

## Reference

String values:

```
${var.foo}
```

Map values:

```
${var.map["key"]}
```

List values:

```
${var.list}

${var.list[index]}
```

Podemos buscar atributos de um recurso:

'TIPO.NOME.ATRIBUTO'

```
${aws_instance.web.id}
```

Ou múltiplos recursos

```
${aws_instance.web.*.id}
```

## Conditionals

${CONDIÇÃO ? TRUEVAL : FALSEVAL}

As interpolation expressions pode ser inseridas em conditions, por exemplo:

```
resource "aws_instance" "vpn" {
    count = "${var.something ? 1 : 0}"
}
```

Se var.something for true, retornamos 1. Caso contrário, 0.

## Concat

Junta duas listas em uma só.

```
concat(aws_instance.db.*.tags.Name, aws_instance.web.*.tags.Name)
```

## Join

Separa uma lista em diversas strings usando um delimitador

```
join(",", aws_instance.foo.*.id)
join(",", var.ami_list)
```

## Contains

Retorna true se uma lista contem um elemento específico. False, caso contrário

```
contains(var.list_of_strings, "an_element")
```

## Merge

Funde dois mapas, criando como se fosse um dicionário

```
${merge(map("a", "b"), map("c", "d"))}
```

O retorno é {"a":"b", "c":"d"}

## Length

```
${length("a,b,c")} = 5
```

## Terraform Console Command

```
terraform console [option] [dir]
```

---

Os exemplos estão em terraform-tutorial/3b_Interpolations_expressions/
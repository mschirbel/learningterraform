# Provisioners

Antes de tudo, precisamos ter um key pair e um SG já preparados.

## Key Pair

Em EC2, clique em Key Pairs e crie uma nova entrada:

![](../images/keypair.jpg)

Agora teremos um arquivo .pem com nossa key.

## Security Group

Podemos editar o SG default mesmo, para as Inbound Rules:

![](../images/security-group.jpg)

## Software Provisioning

Usamos os Provisioners para executar scripts, seja local, ou remoto. Para executar recursos.

Para todos eles, precisamos especificar a Connection String. Exceto quando for local.

Mas isso necessita estar dentro de um recurso. Não pode estar separado de um.

Muitos deles, precisam de acesso remoto a esse recurso, seja SSH ou WinRM

### Connection Arguments

1. Private Key
2. User
3. Password
4. Host
5. Port
6. Timeout

### File Provisioner

Usamos para copiar arquivos de nossa máquina para um recurso recém criado.

#### Dica

Se usarmos como source, DIRETÓRIO_ORIGIM/ -> copiaremos tudo, menos a pasta
Se usarmos como source, DIRETÓRIO_ORIGEM -> copiaremos tudo, até a pasta.

---

## Usando diferentes Provisioners

### Userdata

Tem acesso direto aos detalhes da instância, mas é ruim para checar erros.

### Remote Exec

O terraform cuidado dos erros, mas é muito sequencial, o que pode causar problemas de programação

### Ansible

Tem muito mais recursos para serem trabalhados. Mas adiciona uma camada de conhecimento além do Terraform.

---

## Nginx

É um servidor web para web apps. Podemos usar para proxy, emails e etc.

Por default, usa-se a porta 80.

---

## Demo

Dentro de nosso diretório terraform-tutorial/3h_Software_provisioning_Part_1/

É importante, logo de cara, alterar o arquivo *variables.tf* para as suas credenciais:

```
variable "key_name" {
  default = "keypair"
}

variable "pvt_key" {
  default = "keypair.pem"
}

variable "us-east-zones" {
  default = ["us-east-1a", "us-east-1b"]
}

variable "sg-id" {
  default = "sg-c8f624b1"
}
```

Sendo que, meu arquivo keypair.pem estava no mesmo local do arquivo.

Dentro de nosso arquivo *remote_exec.tf* temos tudo que precisaremos para construir nosso app.

Temos a tag provisioner, do tipo file, que vai copiar a pasta frontend para dentro de nossa instância remota.

E temos outra tag provisioner que vai executar comandos dentro de nossa instância.

Sempre em forma sequencial. Sempre.

Para funcionar:

```
terraform init
terraform apply
```

---

## Ansible

Para instalar o Ansible, podemos usar o script disponibilizado em terraform-tutorial/3i_Software_provisioning_Part_2/scripts

```
chmod +x ansible-install-ubuntu.sh
sudo ./ansible-install-ubuntu.sh
```

Ou caso esteja em outra distro

```
sudo yum install ansible
```

Ansible funciona muito bem com Terraform, porque é Agentless.
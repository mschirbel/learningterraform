# Locals

Local values são nomes para expressões.
Como se fosse um apelido para algo que vc usa repetidamente.

Podemos agrupá-las em blocos, que determinam uma ou mais variáveis.

O nome de um Local precisa ser único

## Exemplo

```
locals {
    instance_ids = ${concat(aws_instance.blue.*.id, aws_instance.gree.*.id)}
}

locals {
    default_name_prefix = "${var.project_name}-web"
    name_prefix = "${var.name_prefix != "" ? var.name_prefix : local.default_name_prefix}"
}
```

## Demo

Dentro do nosso diretório terraform-tutorial/3c_Locals

Temos o arquivo locals.tf
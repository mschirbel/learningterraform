# Workspaces

É um container de estados de Terraform.
E um estado vai definir como os recursos serão geridos baseados no que existem neles.

Com Terraform, sempre começamos com um default Workspace chamado *default*

E não podemos deletá-lo.

Para criar um novo:

```
terraform workspace new foo
```

E para selecionar qual workspace você deseja trabalhar no momento:

```
terraform workspace select
```

E para usar dentro dos templates:

```
${terraform.workspace}
```

## Best Practices

Use diferentes workspaces para diferentes ambientes, como homol, dev, prod.
Mas não use-os como forma de encapsulamento, eles apenas ajudam a controlar diferentes estados. Não é como um namespace no Docker!

## Demo

No diretório terraform-tutorial/3g_Workspaces

Temos o arquivo workspaces.tf.

```
terraform init
```

Mas precisamos, deixar comentado a linha:

```
locals {
  default_name = "${join("-", list(terraform.workspace, "example"))}"
}
```

Pois estamos usando um workspace que não existe ainda.

Agora:

```
terraform workspace list
```

Para ver quais estão criados

```
terraform plan
```

Veja que em tags.Name, temos agora o default-example. Pois é isso que o join fez.

```
terraform workspace new demo
```

Agora veja quais estão criados

```
terraform workspace list
```

E agora 

```
terraform plan
```

Agora em tags.Name temos demo-example. Pois trocamos o workspace que estávamos trabalhando.
# Backends & Remote States

## Backends

Qual será o estado durante a inicialização.

Define como será o apply e o destroy. 
Isso é bom para trabalhar em grandes times.

Para isso, é necessário guardar as informações do estado atual em um local seguro, como um bucket no S3.
E isso torna acessível de diferentes máquinas.

## Remote States

É uma feature do backend.

Por default, o Terraform salva o estado atual em um arquivo *terraform.tfstate*

Com o Remote States, temos uma maneira de salvar o estado, versionar e remote access.

## Demo

Para habilitar o Remote State, devemos ter acesso a um bucket na AWS, por exemplo:

![](Anotação-2019-01-03-011852)

Desse bucket devemos ativar o versionamento e a criptografia.

A criptografia não será de uma notável diferença, mas é uma camada a mais de segurança~.

Agora, voltamos para o diretório terraform-tutorial/3f_Backends_and_remote_states/

```
terraform {
  backend "s3" {
    bucket = "marceloschirbel-terraform-remotestate"
    key    = "network/terraform.tfstate"
    region = "sa-east-1"
  }
}
```

Veja que não inserimos aqui as keys para nosso usuário.

Temos um hidden directory que contem um arquivo de configuração.

```
cd .aws
cat config
cat credentials
```

Agora temos um arquivo para colocar nossas credenciais.

Podemos rodar um

```
terraform init
```

E com isso, teremos nossas credenciais carregadas.

Após um apply, podemos ir ao nosso bucket, e ali veremos uma pasta network e nosso arquivo *terraform.tfstate* com o estado atual de nossa infra.

Esse aquivo pode ser utilizado por diversos membros de um time.
# Terraform

## Introdução

### History

Terraform tem a intenção de mostrar como a terra pode ser modada. Ou como pode ser moldada.

Terraform é sobre moldar a infraestrutura.
Foi lançado em 2014, da mesma empresa de Vagrant - HashiCorp

Foi desenhado para ser uma ferramenta de automação e de configuração. 

Com Terraform podemos codificar todas as partes de nossa infra, seja em um cloud vendor ou em vários.

### O que é IaaC

É a gestão de VMs, Instances, VPC, LB usando códigos de marcação.

Isso não cobre o deployment, que é realizado por ferramentas de automações de deploy - como Ansible, por exemplo.

Podemos usar as mesmas ferramentas de desenvolvimento desde a aplicação até a infraestrutura.

## Como Terraform é diferente de outras ferramentas de automação

Quais outras?

- Puppet
- Chef
- AWS CloudFormation
- Azure Resource Manager
- Boto
- Fog

Chef e Puppet são ferramentas que focam em instalações de softwares em máquinas. Ou seja, a máquina precisa estar funcionando e ter um client instalado. Enquanto que Terraform deseja fazer até o bootstraping da máquina(que não precisa existir ainda)

CloudFormation e Resource Manager são ótimos, mas focam somente em sua própria cloud. Caso seja necessário um multicloud, terá problemas.

Boto e Fog são específicos de alguns cloud vendors e usam API, enquanto que o Terraform usa um acesso em low-level.

## Vantagens de usar Terraform

Podemos orquestrar múltiplos servidores em uma única definição

Podemos usar uma única solução para vários servicos e para várias clouds.

Tem um grande suporte da comunidade.
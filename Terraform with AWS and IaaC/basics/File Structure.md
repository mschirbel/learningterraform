# File Structure

## Fundamentos

A estrutura do Terraform é chamado de HCL - HashiCorp Configuration Language

Essa linguagem tem como objetivo ser de fácil leitura.

Terraform também entende JSON.

A sintaxe permite herarquias, seções e variáveis.

Seções são como mapas, elas controem listas de variáveis.
E o seu uso é recomendado para deixar mais visível e com uma melhor leitura.

## Referências

```hcl
# é um comentário de uma linha
/* */ é o comentário de múltiplas linhas
'key = value' é o modo de atribuição
"strings" é como são as strings
<< EOF para strings com múltiplas linhas
true/false são válidos
["foo","bar"] são listas
{"foo":"bar", "bar":"baz"} são mapas.
```

## JSON

HCL e JSON são interoperantes e a conversão entre eles é muito fácil.

Mas JSON não é tão bom para leitura, pois não temos comentários.

## Common Executions

Sempre começamos com um

```
terraform apply
```

Esse comando verifica se algum plano foi fornecido.

Caso seja fornecido, ele verifica se é para destruir ou construir recursos, isso não dá um feedback

Caso não seja fornecido ele verifica se precisa de um refresh, se precisar, ele faz e acrescenta no plano.

Caso não precise de um refresh, ele constrói o plano.

![](images/Anotação2018-12-21130855.jpg)

## Demo

Caso queira instalar os plugins necessários para um diretório:

```bash
terraform init -upgrade [diretório]
``` 

Agora podemos obter o mesmo resultado de um apply, usando outro comando:

```bash
terraform plan
```

Sem pedir 'yes' ou 'no'

Podemos também:

```
terraform plan -out build-plan
```

Agora podemos ver o plano gerado:

```bash
cat build-plan
```

Claro que não está legível, mas o executável está lá.

Para aplicar esse plano:

```
terraform apply build-plan
```

Sem pedir as autorizações.

Agora, para destruir o plano:

```
terraform plan --destroy -out destroy-plan
```

Para destruir a instância use:

```
terraform apply destroy-plan
```

Mas antes, vamos dar um refresh e um show:

```
terraform refresh
terraform show
```

Com isso, ele vai nos mostrar todo o estado mais atualizado de nossa infra.

Agora a última coisa, é a validação de nosso .tf:

```
terraform validate
```

Se não aparecer nada é porque está correto.
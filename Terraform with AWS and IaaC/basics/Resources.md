# Resources

Resources é o elemento mais importante que vamos configurar, estão em qualquer infraestrutura.

São uma abstração de recursos, sejam de low ou high level.

Por exemplo, um low level seria o tipo de servidor na AWS. High level seria o DNS da aplicação.

A tag de resource sempre virá configurada com o tipo e depois o nome:

```
resource "type" "name"

```

## Meta-parameters

### Count

Configura múltiplas instâncias de um recurso

```
resource "aws_instance" "backend" {
    count = 2
    ami = "ami-65234c1c"
    instance_type = "t2.micro"
}
```

### Depends on

Configura uma dependência entre os recursos

```
resource "aws_instance" "frontend" {
    depends_on = ["aws_instance.backend"]
    ami = "ami-65234c1c"
    instance_type = "t2.micro"
}
```

Assim, a instância de frontend não será criada sem que antes exista a de backend.

### Life cycle

#### Create before destroy

```
lifecycle {
    create_before_destroy = true
}
```

Digamos que um recurso irá ser substituído. Isso vai prevenir que o antigo seja destruido antes que o novo esteja em funcionamento.

#### Prevent Destroy

```
lifecycle {
    prevent_destroy = true
}
```

### Timeouts

Configura um tempo que o recurso tem para que seja tomada uma ação:

```
timeouts {
    create = "60m"
    delete = "2h"
}
```

## Demo

Vamos criar um servidor de frontend com dois de backend.

Dentro da pasta terraform-tutorial/2d_Resources/resources.tf

OBS: as permissões não podem permitir múltiplas escritas no arquivo .tf

```
sudo chmod 700 resources.tf
terraform apply
```
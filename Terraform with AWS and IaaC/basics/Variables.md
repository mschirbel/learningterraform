# Variables

Variáveis server como parâmetros para módulos do Terraform.

Isso permite que a configuração fique ainda mais versionável.

## Como usar

Variáveis devem ser escritas em blocos:

```
variable "key" {
    type = "string"
}

variable "images" {
    type = "map"
}
```

Quais parâmetros existe:

1. type
2. default
3. description

type pode ser 'string' ou 'list' ou 'map'

default caso não seja passado nenhum valor, vai ser escolhido esse. Caso não exista default, somos obrigados a passar um parâmetro para a variável.

description cria uma descrição para quando o módulo for publicado.

## Exemplos

```
variable "key" {
    type = "string"
    default = "value"
    description = "usado para ser uma chave"
}

variable "users" {
    type = "list"
    default = ["admin", "centos"]
}

variable "images" {
    type = "map"
    default = {
        "us-east-1" = "image-1234"
        "us-west-2" = "image=4567"
    }
}

variable "active" {
    default = false
}
```

## Var Files

Podemos criar um arquivo para conter todas as variáveis.

E podemos passá-lo ao comando:

```
terraform apply -var-file=foo.tfvars
```

A extensão do arquivo é .tfvars

E podemos ter mais de um arquivo no mesmo comando.

## Example

Dentro da pasta terraform-tutorial/2f_Variables/variables.tf temos nosso arquivos de variáveis e como será nossos recursos na AWS

```
terraform plan
```

Veja que após executar o plano, veremos a diferença nas AZ de cada uma das duas instâncias, isso ficou setado em nossa variável zones.

Mas caso seja necessário separar isso, temos nosso arquivo var-file-example.tfvars

```
terraform plan -var-file=var-file-example.tfvars
```

Veja que as AZ agora são as que setamos no arquivo var-file-example.tfvars.

Agora, caso queira que as variáveis sejam exportadas para todos os arquivos como default:

```
export TF_VAR_zones='["us-east-1d", "us-east-1c"]'
```

Para executar usando outros parâmetros diferente dos default

```
terraform apply -var 'zones=["us-east-1d", "us-east-1c"]'
```
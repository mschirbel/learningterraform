# Output

São valores que o Terraform considera imporante, e mostra pra você na tela quando vc usa um

```
terraform apply
```

por exemplo.

## Atributos

Suponha que existam mais de 20 recursos. Obviamente não vamos querer todo o output sendo exibido em nossa tela, somente algumas coisas mais importantes.

O bloco sempre será da seguinte forma:

```
output NAME {
    value = VALUE
}
```

Podemos ter vários dentro do mesmo bloco.

## Parâmetros

Temos o value, que receberá uma string, ou map ou listas.
Description que será uma string
Depends_on que será dependências entre os outputs
Sensitve = boolean

## Exemplo

```
output "frontend_ip" {
    value = "${aws_instance.frontend.public_ip}"
}

output "backend ips" {
    value = "${aws_instance.backend.*.public_ip}"
}
```

No primeiro exemplo, estamos pegando o IP de uma instância.
No segundo exemplo estamos pegando os IPs de múltiplas instâncias.]

Após executar o apply, o output será exibido.
Caso queira ver novamente

```
terraform output
```
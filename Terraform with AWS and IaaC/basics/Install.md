# Instalação

## Unix & Mac

Nesse curso, utilizaremos um Centos 7.5

```bash
sudo yum install wget unzip
```

Para baixar a última versão do Terraform use o seguinte comando:

```sh
wget https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
```

Ou pode entrar no site deles e baixar por ![aqui](https://www.terraform.io/downloads.html)

Após o download:

```bash
unzip terraform_0.11.8_linux_amd64.zip
sudo cp terraform /usr/local/bin
sudo chmod +x /usr/local/bin
```

Para verificar a versão do Terraform:

```
terraform --version 
```

## Windows

Para fazer o download basta entrar no site deles e baixar por ![aqui](https://www.terraform.io/downloads.html)

Também criaremos uma nova pasta no disco C:

E nessa pasta jogaremos os arquivos que baixamos no site do Terraform.

Depois basta inserir o local de instalação do terraform dentro do PATH.
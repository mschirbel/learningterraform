# Providers

Cuidam do lifecycle de um recurso - CRUD: create, read, update, delete.

## Outline

Para fazer um outline:

```
provider "aws" {
    access_key = "ACCESS KEY"
    secred_key = "SECRET KEY"
    region = "sa-east-1"
}
```

Sempre vem:

provider "tipo" {
    propriedades daquele tipo de provider
    ...
}

Para ler mais sobre as propriedades, clique ![aqui](https://www.terraform.io/docs/providers/index.html)

Providers geralmente são aqueles que tem APIs, como empresas que geram algum produto.

## Initialization

Para adicionar um novo provider, primeiro precisa inicializá-lo. Isso requer instalar a ultima versão.
Com um init do terraform, isso já fica pronto

```
terraform init
```

## Multi Provider Instance

É possível usar diferentes contas, ou diferentes regiões dentro do mesmo provider. Mesmo que usando keys diferentes.

Para realizar um deploy, temos um exemplo dentro de terraform-turorial/2e_Providers
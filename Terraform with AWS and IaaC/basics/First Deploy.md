# First Deploy

## Pré-Requisitos

- Terraform instalado
- Clone do github LevelUpEducation/terraform-tutorial
- AWS Account
- AWS Access key
- AWS Secret key

## Configuration

Todos os arquivos de terraform terminam com .tf

O arquivo está em terraform-tutorial/1f_First_terraform_deployment/first_deployment.tf

Um fato importante é que o Terraform vai escrever alguns arquivos dentro das pastas do arquivo. Pra isso, as permissões devem estar corretas.

Também guarda todos os IDs dos recursos criados, podemos ver isso usando:

```
terraform show
```

Dentro do Console da AWS, vá em IAM -> Users e crie um novo usuário. 
Dentro de Users, clique no usuário criado e vá em Create Access Key

Agora entre na pasta terraform-tutorial/1f_First_terraform_deployment/

```
terraform init
```

Após isso, substituimos as keys dentro do arquivo e usamos

```
terraform apply
```

Perceberemos que todas as ações que serão feitas são exibidas na tela.
E podemos digitar "yes" em "Enter a value"

Agora a cada 10s receberemos uma notificação vendo o que o Terraform está fazendo.

Se formos ao console da AWS, veremos nossa instância criada:

![](images/Anotação2018-12-20234548.jpg)

E o terraform nos retornará uma mensagem similar a essa:

```
aws_instance.example: Creation complete after 34s (ID: i-0c7b264d76017770c)

Apply complete! Resources: 1 added, 0 changed, 0 destroyed
```

Agora, se quisermos ver o que foi feito:

```
terraform show
```

Mas não precisaremos dessa instância por enquanto, para apagar tudo o que foi feito:

```
terraform destroy
```
provider "aws" {
    access_key = "AKIAJ7SDYNU5EKWFX6CA"
    secret_key = "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"
    alias      = "us-east-1"
    region     = "us-east-1"
}

provider "aws" {
    access_key = "AKIAJ7SDYNU5EKWFX6CA"
    secret_key = "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"
    alias      = "us-west-1"
    region     = "us-west-1"
}

variable "zones-east" {
  default = ["us-east-1a", "us-east-1b"]
}

variable "zones-west" {
  default = ["us-west-1a", "us-west-1b"]
}

resource "aws_instance" "east-frontend" {
    provider      = "aws.us-east-1"
    count         = 2
    availability_zone     = "${var.zones-east[count.index]}"
    ami           = "ami-07585467"
    instance_type = "t2.micro"
    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_instance" "east-backend" {
    provider      = "aws.us-east-1"
    count         = 2
    availability_zone     = "${var.zones-east[count.index]}"
    ami           = "ami-07585467"
    instance_type = "t2.micro"
    lifecycle {
        prevent_destroy = true
    }
}

resource "aws_instance" "west-frontend" {
    provider      = "aws.us-west-1"
    count         = 2
    availability_zone     = "${var.zones-west[count.index]}"
    ami           = "ami-66506c1c"
    instance_type = "t2.micro"
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_instance" "west-backend" {
    provider      = "aws.us-west-1"
    count         = 2
    availability_zone     = "${var.zones-west[count.index]}"
    ami           = "ami-66506c1c"
    instance_type = "t2.micro"
    lifecycle {
        prevent_destroy = true
    }
}

output "backend_ip-east" {
  value = "${aws_instance.east-backend.*.public_ip}"
}

output "backend_ip-west" {
  value = "${aws_instance.west-backend.*.public_ip}"
}

output "frontend_ip-east" {
  value = "${aws_instance.east-frontend.*.public_ip}"
}

output "frontend_ip-west" {
  value = "${aws_instance.west-frontend.*.public_ip}"
}
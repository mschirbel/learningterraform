# Kubernetes

Vamos ver o que é Kubernetes, como fazer um deploy de um cluster na GCP.

Vamos também estudar sobre o provider de Kubernetes

## Introduction

É uma ferramenta open-source que tem como objetivo automatizar o deploy e scaling de containers.

Kubernetes vem do grego, que é a pessoa que conduz um navio, por isso o símbolo é um timão.

## Onde podemos rodar o kubernetes

1. cloud - seja privada ou publica
2. linux
3. mac
4. windows
5. hybrid system
6. multi cloud

## Por que usar Kube?

1. É o lider de mercado
2. tem uma comunidade enorme
3. tem capacidade para crescer
4. nao precisamos mais pensar em servidores individuais

## Features

1. storage
2. secrets
3. scaling
4. loggin
5. debuggin
6. monitoring
7. load balancing
8. updates

---

## Pré-requisitos 

1. GCP SDK - com autentiração
2. GCP project
3. GKE API

## Google Cloud

A GCP pode ser acessada através do seguinte [https://cloud.google.com](link).

E automaticamente somos enviado para um projeto.

O primeiro passo é sempre olhar o Billing, ou Faturamento:

![](../images/gcp-faturamento.jpg)

Adicione um cartão de crédito para vincular um projeto.

## Criar um projeto via SDK

Para baixar o SDK acesse o [https://cloud.google.com/sdk/downloads](link)

É como se fosse uma linha de comando para o GCP.

Assim que estiver instalado. Vá ao diretório raiz do projeto e digite

```
gcloud init
```

E siga as instruções para logon e para criar projetos.

## Deploy de Cluster

Usamos o terraform para alterar a GCP pois temos um controle maior no versionamento do Kubernetes.
E também temos um lifecycle e um feedback maior.

GKE(Google Kubernetes Engine) é uma forma de fazer o deploy, controle e scaling de containers usando a GCP.

### Node Pools

São um conjunto de máquinas configuradas do mesmo jeito e que são conectadas ao Master. O Master vai controlar os pods e os containers dentro de cada uma.

Podemos especificar a quantidade de CPU, memória e as imagens dos containers.

## Demo

Vamos para o console e vamos na seção do GKE:

![](../images/gcp-gke.jpg)

Selecionamos o projeto que queremos usar. E habilitamos o Biling para o projeto.

Clique em Create a Cluster.

Dê um nome e as configurações do node pool:

![](../images/gke-config-cluster1.jpg)

Aqui também indicamos a imagem de cada container e a quantidade.

Para fazer o deploy, vamos em Worloads:

![](../images/workloads-gcp.jpg).

---

Temos um arquivo dentro de terraform-tutorial/5c_Deploy_cluster

Chamado kube.tf. Nesse arquivo temos como fazemos uma conexão com a GCP e conectamos a um projeto.

Dentro da pasta terraform-tutorial/5c_Deploy_cluster/kubernetes/

Temos os arquivos que vão criar a nossa app na GCP.

Agora basta:

```
terraform init
terraform apply
```

E dentro do console poderemos ver nosso cluster com todos os detalhes.

---

## Pods & Services

Container sempre funcionam em pods. Um pod é a parte mais simples do kubernetes.
Pods tem uma rede e um storage distribuído. Ou seja, caso queira algo na mesma porta, não vai funcionar.

Por boas práticas, um container por pod!

Cada Pod vai ter o seu próprio IP e eles dividem as portas entre si, ou seja, não podem ter a mesma porta sendo usada ao mesmo tempo.

Pods são efêmeros, quando algum morrer, provavelmente outro IP será usado. 

Para isso, usamos os Services, que são uma espécie de LoadBalancers.
Eles oferecem IPs estáveis e DNS

Podemos colocar labels em nossos pods e assim selecioná-los.

## Deploy

A primeira coisa a se fazer, é copiar a connection string que temos no exemplo 5c.

Depois preicsamos conectar nosso kube com a API da GCP.

Depois podemos ir na pasta do deploy e usar um

```
terraform apply
```


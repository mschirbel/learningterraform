# Lambda

É um serviço que comporta diversas linguagens de programação.

É um serviço serverless, ou seja, você sobe o seu código, que funciona como trigger para outros serviços da AWS.

Isso funciona sem subir nenhum servidor.

E isso é pago pela demanda, ou seja, cada requisição. Isso reduz muito o custo total do projeto.

## Demo

Em terraform-tutorial/4i_Lambda/ 

Temos um exemplo de código Python para uma função Lambda.

```
import os

def lambda_handler(event, context):
    return "{} from Lambda!".format(os.environ['greeting'])
```

Para carregar um arquivo:

```
data "archive_file" "zip" {
  type        = "zip"
  source_file = "hello_lambda.py"
  output_path = "hello_lambda.zip"
}
```

E para criar a Lambda Function

```
resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = "${data.aws_iam_policy_document.policy.json}"
}

resource "aws_lambda_function" "lambda" {
  function_name = "hello_lambda"

  filename         = "${data.archive_file.zip.output_path}"
  source_code_hash = "${data.archive_file.zip.output_sha}"

  role    = "${aws_iam_role.iam_for_lambda.arn}"
  handler = "hello_lambda.lambda_handler"
  runtime = "python3.6"

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}

```
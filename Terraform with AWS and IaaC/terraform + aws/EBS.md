# EBS

É um espaço de storage virtualizado na AWS. Como se fosse um disco, que pode ser adicionado a uma instâcia.

Downtime, latency são diminuídas ao usar um EBS.

É ideal para se usar em produção, mas não para guardar dados.

```
resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.example.id}"
  instance_id = "${aws_instance.web.id}"
}

resource "aws_instance" "web" {
  ami               = "ami-21f78e11"
  availability_zone = "us-west-2a"
  instance_type     = "t1.micro"

  tags {
    Name = "HelloWorld"
  }
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-west-2a"
  size              = 100
}

```
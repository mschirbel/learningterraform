# AutoScaling

É uma feature, que permite criar novas instâncias a partir de métricas, como uso de CPU, memória, disco e etc.

Serve para aumentar ou para diminuir o grupo de instâncias.

Sempre temos que ter um Launch Configuration, antes de criar o Autoscaling group. Sobre ela que será feito o scaling.

Veja em terraform-tutorial/4f_AutoScaling o arquivo .tf para mais detalhes.
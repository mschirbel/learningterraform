# VPC

VPCs são virtual networks. Por default, toda conta já começa com uma. Mas podemos criar até 5 em uma conta.

Podemos controlar e isolar nossas instâncias, na camada de network. 
Podemos criar subnets, que são ranges de IPs, públicos ou privados.

## Internet Gateway

É uma porta de saída da nossa VPC para a Internet.

## Demo

Dentro do arquivo terraform-tutorial/4b_Vpcs/vpc.tf temos um exemplo de como criar uma VPC na AWS.

O jeito mais fácil é usar o provider aws com o módulo vpc.
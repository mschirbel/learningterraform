# IAM

É o serviço de controle de acesso da AWS.

Podemos configurar os SG, users, groups, e policies.

Podemos escrever as policies dentro do terraform:

```
resource "aws_iam_group_policy" "policy" {
  name  = "policy"
  group = "${aws_iam_group.group.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

```
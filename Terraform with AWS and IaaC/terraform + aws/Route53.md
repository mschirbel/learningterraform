# Route53

É o serviço de DNS da AWS.

Após a compra de um domínio, podemos declará-lo da seguinte forma:

```
resource "aws_route53_zone" "primary" {
  name = "my-example.com"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "www.my-example.com"
  type    = "A"
  ttl     = "300"
  records = ["198.40.6.7"]
}
```

Primeiro, criamos a zona de route53, e declaramos o DNS que compramos.

Depois, colocamos cada record que teremos para nossa URL.

Por exemplo, www.[meu dns].com.br ou teste.[meu dns].com.br ou batata.[meu dns].com.br

Colocamos em *name* o DNS completo e em *records* os IPs para qual será redirecionado.